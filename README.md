# **Db**

Postgres db for spixel.

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/db/src/master/LICENSE.md)**.
